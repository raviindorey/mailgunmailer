require 'test_helper'
class UsersControllerTest < ActionDispatch::IntegrationTest 
	include Devise::Test::IntegrationHelpers

	test "should get index" do
		sign_in users(:ravi)
		get users_index_url
		assert_response :success
	end

	test "should get at leat one user at index page" do
		sign_in users(:ravi)
		get users_index_url
		assert_select 'h3'
	end

	test "should sign in before mailing" do
		post send_email_url
		assert_redirected_to new_user_session_path
	end

	test "should show Edit Profile only when logged in" do
		get edit_user_registration_url
		assert_redirected_to new_user_session_path
	end

end
