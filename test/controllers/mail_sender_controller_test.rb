require 'test_helper'

class MailSenderControllerTest < ActionDispatch::IntegrationTest
  
	include Devise::Test::IntegrationHelpers
	
	test "should get show(root)" do
		sign_in users(:ravi)
		get root_path
		assert_response :success
	end

	test "should have sender's email" do
		sign_in users(:ravi)
		get root_path
		assert_select 'td', "#{users(:ravi).email}", {count: 1} 
	end

end
