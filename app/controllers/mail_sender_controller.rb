class MailSenderController < ApplicationController
  def show
  	@users = User.all
  end

  def send_email
  	if Rails.env.production?
  		begin
	  		RestClient.post "https://api:#{ENV['MAILGUN_API_KEY']}@api.mailgun.net/v2/#{ENV['MAILGUN_DOMAIN']}"+"/messages",
	  		    :from => "#{current_user.email}",
	  		    :to => "#{params[:to_email]}",
	  		    :subject => "#{params[:subject_email]}",
	  		    :text => "#{params[:content_email]}",
	  		    :html => "#{params[:content_email]}"
	  		flash[:success] = "Email Sent"
	  	rescue Exception => err
	  		flash[:danger] = "#{err}"
	  	end
  	else
  		flash[:info] = "Email is configured only for the production env."
  	end 
	redirect_to root_url
  end

end
