Rails.application.routes.draw do
  
  devise_for :users
  root 'mail_sender#show'
  post 'send_email' => 'mail_sender#send_email'
  get 'users/index'
end